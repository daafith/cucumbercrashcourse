package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
                plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"}, // Standard formatting.
                features = { "src/test/resources/features" }, // Where are the features?
                glue = { "examples" }, // Where are the step definitions (package)?
                tags={"@hello","~@ignore"} // Which tags should or should not (~) be run.
                )
public class RunCukesIT {
        
}
