package examples.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class BrowserEventListener implements WebDriverEventListener {

  public void afterChangeValueOf(WebElement driver, WebDriver arg1) {
    // No need for implementation yet
  }

  @Override
  public void beforeNavigateTo(String url, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterNavigateTo(String url, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeNavigateBack(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterNavigateBack(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeNavigateForward(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterNavigateForward(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeNavigateRefresh(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterNavigateRefresh(WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeFindBy(By by, WebElement element, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterFindBy(By by, WebElement element, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeClickOn(WebElement element, WebDriver driver) {
    highlightElement(element, driver);
  }

  @Override
  public void afterClickOn(WebElement element, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
    highlightElement(element, driver);
    
  }

  @Override
  public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void beforeScript(String script, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void afterScript(String script, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void onException(Throwable throwable, WebDriver driver) {
    // TODO Auto-generated method stub
    
  }

  private void highlightElement(WebElement element, WebDriver driver) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
        "color: red; border: 2px solid red;");
  }


}
