package examples.utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import examples.utils.BrowserFactory.Browser;

public class WebHooks {
  
  public static WebDriver getDriver() {
    return driver;
  }

  private static WebDriver driver;

  @Before("@web")
  public void setUp() {
    driver = BrowserFactory.createBrowser(Browser.CHROME);
    driver.manage().window().maximize();
  }

  @After("@web")
  public void tearDown(Scenario scenario) {
    if (scenario.isFailed()) {
      try {
        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
      } catch (WebDriverException wde) {
        System.err.println(wde.getMessage());
      } catch (ClassCastException cce) {
        cce.printStackTrace();
      }
    }
    driver.quit();
  }


}
