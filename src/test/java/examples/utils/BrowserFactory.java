package examples.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.google.common.annotations.Beta;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

/**
 * Creates a browser and returns its driver so that further interaction is possible.
 *
 */
public class BrowserFactory {

  public enum Browser {
    CHROME,
    @Beta
    FIREFOX;
  }

  public static WebDriver createBrowser(Browser browser) {
    switch (browser) {
    case CHROME:
      return createChromeBrowser();
    default:
      return createFireFoxBrowser();
    }
  }

  private static EventFiringWebDriver createFireFoxBrowser() { 
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities = DesiredCapabilities.firefox();
    FirefoxDriverManager.getInstance().setup();
    return new EventFiringWebDriver(new FirefoxDriver(capabilities)).register(new BrowserEventListener());
  }


  private static EventFiringWebDriver createChromeBrowser() {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities = DesiredCapabilities.chrome();
    capabilities.setCapability("chrome.switches", "--verbose");
    ChromeDriverManager.getInstance().setup();
    return new EventFiringWebDriver(new ChromeDriver(capabilities)).register(new BrowserEventListener());
  }

}

