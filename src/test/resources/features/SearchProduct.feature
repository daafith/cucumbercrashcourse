@web
Feature: Search for products in the web shop
	
	As a B
	I want to C
	So that D

	Scenario Outline: I am going to find products
	Given I am on the home page of the Polteq test shop
	When I search for <product>
	Then the results contains the name of my <product>
		And there are at least <amount> result(s)
		
		Examples: products and amounts
		|product	|amount	|
		|"iPod"		|5		|
		|"macbook"	|2		|
		
		
#	Scenario: I am not going to find anything!
#	Given I am on the home page of the Polteq test shop
#	When I search for "mountainbikemoersleutel"
#	Then I get a message telling me it can not be found